# OnePlus 6-6T HEVC Recording - With half of the bitrate

This module enable HEVC/H265 video recording on OnePlus 6 and OnePlus 6T, install it via Magisk Manager. The bitrate values are about half of the original H264 and the HEVC code it's enabled only for all the video profiles that previously used H264.
The audio change too, from AAC to HEAAC since at the default audio bitrate (96 kb/s) HEAAC provide a better quality (and compression).

## Bitrate reduction table

Since the average bitrate reduction it's about 59%, in this fork I have reduced the bitrate to about 50% to save space

| Codec | 480p | 720p | 1080p | 2160p|
|:-----:|:----:|:----:|:-----:|:----:|
| HEVC  | 52%  | 56%  | 62%   | 64%  |

<sup>Source [Wikipedia](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding#Coding_efficiency)</sup>
